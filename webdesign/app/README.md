# Desafio Joyjet

Este desafio foi feito para analisar meu conhecimento, usando recursos específicos.

## Tecnologias utilizadas

- ReactJS
- Webpack
- CSS3
- SCSS
- SVG
- Bootstrap 4 + SCSS

## Executando o projeto

A aplicação está dentro desta pasta `app` e é necessário ter instalados o [Node.js](https://nodejs.org/) e também o [Yarn](https://yarnpkg.com/).

Instale as dependências através do comando:

```
yarn
```

Rode a aplicação usando o comando:

```
yarn start
```

Depois disso, abra a url `http://localhost:3000/` em seu navegador.
