module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  jsxSingleQuote: true,
  singleQuote: true,
  trailingComma: 'es5',
  semi: false,
}
