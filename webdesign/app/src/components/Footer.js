import React from 'react'
import { Container } from 'react-bootstrap'

const Footer = () => (
  <footer className='footer'>
    <Container>
      <p>
        © 2016 Created by Joyjet <span>Digital Space Agency</span>
      </p>
    </Container>
  </footer>
)

export default Footer
