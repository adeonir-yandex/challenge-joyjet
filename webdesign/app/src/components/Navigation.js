import React, { useEffect } from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap'

import Logo from '../assets/images/space-logo.svg'

const Navigation = () => {
  function handleScroll() {
    const className = 'scrolled'
    const element = document.querySelector('.navbar')

    if (window.scrollY > 50) {
      element.classList.add(className)
    } else {
      element.classList.remove(className)
    }
  }

  function handleBackground() {
    const className = 'background'
    const element = document.querySelector('.navbar')

    element.classList.toggle(className)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  return (
    <Navbar fixed='top' expand='lg' role='navigation' variant='dark'>
      <Container>
        <Navbar.Brand href='/'>
          <Logo />
        </Navbar.Brand>
        <Navbar.Toggle onClick={handleBackground}>
          <span aria-hidden='true'></span>
          <span aria-hidden='true'></span>
          <span aria-hidden='true'></span>
          <span aria-hidden='true'></span>
        </Navbar.Toggle>
        <Navbar.Collapse>
          <Nav className='ml-auto'>
            <Nav.Item>
              <Nav.Link href='#' eventKey='blog'>
                Blog
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href='#' eventKey='popular'>
                Popular
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href='#' eventKey='archive'>
                Archive
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href='#' eventKey='about'>
                About
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default Navigation
