import React from 'react'
import { Container, Row, Col, Card } from 'react-bootstrap'

const news = [
  {
    title: 'International Space Station',
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur luctus aliquet sapien….',
    image: '/img/international-space-station.jpg',
  },
  {
    title: 'My Capsule',
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur luctus aliquet sapien….',
    image: '/img/my-capsule.jpg',
  },
  {
    title: 'My Moon',
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur luctus aliquet sapien….',
    image: '/img/my-moon.jpg',
  },
]

const Main = () => (
  <main>
    <section className='news'>
      <Container>
        <Row>
          {news.map((card, index) => (
            <Col key={index}>
              <Card className='border-0'>
                <Card.Img variant='top' src={card.image} />
                <Card.Body>
                  <Card.Title>{card.title}</Card.Title>
                  <Card.Text>{card.text}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </section>

    <section className='about'>
      <Container>
        <Row>
          <Col sm={4} className=''>
            <img
              src='/img/about-us.jpg'
              alt='My Capsule'
              className='rounded mw-100 mb-md-4'
            />
          </Col>
          <Col>
            <h3 className='text-primary font-weight-bold text-uppercase mb-4'>
              About Us
            </h3>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
              commodo ligula eget dolor. Aenean massa. Cum sociis natoque
              penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
              sem. Nulla consequat massa quis enim. Donec pede justo, fringilla
              vel, aliquet nec, vulputate eget, arcu.
            </p>
            <p>
              In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
              Nullam dictum felis eu pede mollis pretium. Integer tincidunt.
              Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate
              eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae,
              eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis,
              feugiat a, tellus. Phasellus viverra nulla ut metus varius
              laoreet. Quisque rutrum.
            </p>
          </Col>
        </Row>
      </Container>
    </section>
  </main>
)

export default Main
