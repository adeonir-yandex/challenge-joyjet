import React from 'react'
import { Jumbotron, Container, Row, Col, Button } from 'react-bootstrap'

const Header = () => {
  return (
    <Jumbotron fluid>
      <header className='header'>
        <Container>
          <Row>
            <Col lg={6} md>
              <h1 className='display-1'>
                Space<span></span>
              </h1>
              <p className='lead'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
                molestie elit at lacus…
              </p>
              <Button variant='primary'>Click</Button>
            </Col>
          </Row>
        </Container>
      </header>
      <section className='trending'>
        <Container>
          <Row noGutters>
            <Col className='font-weight-bold text-right'>
              Trending <span className='text-primary d-block'>Today</span>
            </Col>
            <Col>
              Lorem ipsum dolor sit amet, consectetuer adipiscing ligula eget
              dolor.
            </Col>
            <Col>
              Lorem ipsum dolor sit amet, consectetuer adipiscing ligula eget
              dolor.
            </Col>
            <Col>
              Lorem ipsum dolor sit amet, consectetuer adipiscing ligula eget
              dolor.
            </Col>
          </Row>
        </Container>
      </section>
    </Jumbotron>
  )
}

export default Header
