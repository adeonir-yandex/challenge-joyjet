import React from 'react'

import './App.scss'

import Navigation from './components/Navigation'
import Header from './components/Header'
import Main from './components/Main'
import Footer from './components/Footer'

const App = () => (
  <>
    <Navigation />
    <Header />
    <Main />
    <Footer />
  </>
)
export default App
